# Technology Stack: 
Java, Maven, Git, Cucumber BDD, Selenium, Serenity, IntelliJ IDEA

# Usage:
Framework will be used to perform regression as well as functional testing.

# Product:
Google Website - main page

# Automation flow:
The validation files is write in cucumber methodology to better understand the requirement and purpose of testing.
Test use xpath mapping to select, display and use functions in testing page based on Selenium documentations.    

# Test Execution:
To execute all feature files, need to run terminal and then run maven command: "mvn clean verify" in the root of the project.

# Test Results:
Test result are displayed in html file build based on Serenity library.
After execute features file, report will be generated under this path: "target/site/serenity/index.html"

# Requirements:
Chrome browser with ver. 95.0.*,
JDK 11
