Feature: Purpose of this test is verify visibility and functionality features in main page

  Scenario Outline: Check basic hyperlink button in main page
    Given Open google website
    When Check visibility and available <object>
    And Click <object>
    Then Verify new opened page
    Examples:
    | object             |
    | 'GMAIL'            |
    | 'GRAPHIC'          |
    | 'ACCOUNT_LOGIN'    |
    | 'ABOUT'            |
    | 'ADVERTISING'      |
    | 'BUSINESS'         |
    | 'HOW_SEARCH_WORKS' |
    | 'PRIVACY'          |
    | 'TERMS'            |

    Scenario: Check basic menu option under button
      Given Open google website
      When Check visibility and available 'APPS_OPTION_GOOGLE'
      And Click 'APPS_OPTION_GOOGLE'
      Then Check visibility apps and names
      Given Open google website
      When Check visibility and available 'SETTINGS'
      And Click 'SETTINGS'
      Then Verify new opened page under 'SETTINGS'



