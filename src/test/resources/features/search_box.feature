Feature: Check if search features are available and functional on Web Page

  Scenario: Check if search field for normal values
    Given Open google website
    When Write 'test' in 'SEARCH_BOX'
    And Click 'SEARCH_BUTTON'
    Then Check if title page is as 'test'
    And Verify exist stats result after searching
    Given Open google website
    When Write 'null' in 'SEARCH_BOX'
    And Click 'SEARCH_BUTTON'
    Then Check if title page is as 'null'
    And Verify exist stats result after searching

  Scenario Outline: Check if search field for empty values
    Given Open google website
    When Write <text> in 'SEARCH_BOX'
    And Click 'SEARCH_BUTTON'
    Then Check if title page is as 'Google'
    Examples:
      | text  |
      | ''    |
      | ' '   |

  Scenario: Check if list search is display and suggest "Morgan Freeman"
    Given Open google website
    When Write 'Morgan f' in 'SEARCH_BOX'
    And Check if list search display 'Morgan Freeman' and click
    Then Check if title page is as 'morgan freeman'
    And Verify exist stats result after searching

  Scenario Outline: Check if value can be deleted after click cross button
    Given Open google website
    When Write <text> in 'SEARCH_BOX'
    And Click 'CROSS_BUTTON'
    Then Check if 'SEARCH_BOX' is empty
    Examples:
      | text                |
      | 'something wrong'   |
      | ' '                 |

    Scenario Outline: Check if lucky search display directly page
      Given Open google website
      When Write <planet> in 'SEARCH_BOX'
      And Click 'LUCKY_BUTTON'
      Then Check if <web_page> is display
      And Check if title page is as <planet>
      Examples:
      | planet    | web_page                                |
      | 'Uranus'  | 'https://en.wikipedia.org/wiki/Uranus'  |
      | 'Neptune' | 'https://en.wikipedia.org/wiki/Neptune' |