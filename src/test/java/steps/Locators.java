package steps;

public enum Locators {
    GMAIL("//a[contains(@href,'mail.google.com')][@target='_top']"),
    GRAPHIC("//a[contains(@href,'imghp?')][@target='_top']"),
    APPS_OPTION_GOOGLE("//a[contains(@href,'about/products')][@role='button']"),
    LIST_APPS_GOOGLE1("//c-wiz//div/ul[1]/li"),
    LIST_APPS_GOOGLE2("//c-wiz//div/ul[2]/li"),
    ACCOUNT_LOGIN("//a[contains(@href,'accounts.google.com')][@target='_top']"),
    BANNER("//img[@alt='Google']"),
    SEARCH_BOX("//div/input[@type='text']"),
    SEARCH_BUTTON("(//div//input[@type='submit'])[3]"),
    LUCKY_BUTTON("(//div//input[@type='submit'])[4]"),
    ABOUT("//a[contains(@href,'about.google')]"),
    ADVERTISING("//a[contains(@href,'ads/?')]"),
    BUSINESS("//a[contains(@href,'services/?')]"),
    HOW_SEARCH_WORKS("//a[contains(@href,'howsearchworks/?')]"),
    PRIVACY("//a[contains(@href,'policies.google.com/privacy')]"),
    TERMS("//a[contains(@href,'policies.google.com/terms')]"),
    SETTINGS("//div/button[contains(@jsaction,'mousedown')]"),
    POPUP_ADVERTISEMENT_AGR("//div[@role='dialog']//span/div"),
    POPUP_BUTTON_ACCEPT("//div[@class='VDity']/button[2]"),
    CROSS_BUTTON("//div/span[@role='button']"),
    RESULT_STATS("//div[@id='result-stats']"),
    LIST_BOX("//div/ul[@role='listbox']"),
    LIST_BOX_OPTIONS("//ul[@role='listbox']//div[1]/span");

    private String xpath;

    Locators(String xpath){
        this.xpath = xpath;
    }


    public String getXpath() {
        return xpath;
    }
}
