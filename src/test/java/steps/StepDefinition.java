package steps;

import io.cucumber.java.Before;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.cucumber.java.After;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;


import java.time.Duration;

import static org.testng.Assert.*;

public class StepDefinition {
    private WebDriver driver;
    private WebDriverWait wait;
    private final static String PAGE = "https://www.google.pl";
    Logic logic = new Logic();

    @Before()
    public void openBrowser(){
        System.setProperty("webdriver.chrome.driver", "src/test/java/resources/driver/chromedriver.exe");
        driver = new ChromeDriver();
        wait = new WebDriverWait(driver, Duration.ofSeconds(2));

    }
    @After()
    public void closeBrowser() {
        driver.close();
        driver.quit();
    }

    @Given("Open google website")
    public void openGoogleWebsite() throws InterruptedException {
        driver.navigate().to(PAGE);
        logic.checkPopUpCookie(driver);
    }

    @When("Write {string} in {string}")
    public void writeTextIn(String text, String locator) {
        driver.findElement(By.xpath(Locators.valueOf(locator).getXpath()));
        driver.findElement(By.xpath(Locators.valueOf(locator).getXpath())).sendKeys(text);
    }

    @Then("Check if title page is as {string}")
    public void checkIfTitlePageIsAsText(String text) {
            String pageTitle = driver.getTitle();
            assertTrue(pageTitle.contains(text));
    }

    @And("Verify exist stats result after searching")
    public void verifyExistStatsResutAfterSearching() {
        boolean exist = driver.findElement(By.xpath(Locators.RESULT_STATS.getXpath())).isDisplayed();
        assertTrue(exist);
    }

    @And("Check if list search display {string} and click")
    public void checkIfListSearchDisplayAndClick(String name) {
        driver.findElement(By.xpath(Locators.SEARCH_BOX.getXpath())).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.LIST_BOX.getXpath())));
        logic.findValueFromListSearchAndClick(driver, name);
    }

    @And("Click {string}")
    public void clickButton(String string) {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.valueOf(string).getXpath())));
        driver.findElement(By.xpath(Locators.valueOf(string).getXpath())).click();
    }

    @Then("Check if {string} is empty")
    public void checkIfBoxIsEmpty(String locator) {
        String empty = "";
        String val = driver.findElement(By.xpath(Locators.valueOf(locator).getXpath())).getText();
        System.out.println(val);
        assertEquals(val,empty);
    }

    @Then("Check if {string} is display")
    public void checkIfWebPageIsDisplay(String expectPage) {
        String actualPage = driver.getCurrentUrl();
        assertEquals(actualPage, expectPage);
    }

    @When("Check visibility and available {string}")
    public void checkVisibilityAndAvailableGMAIL(String locator) {
        boolean displayed = driver.findElement(By.xpath(Locators.valueOf(locator).getXpath())).isDisplayed();
        boolean enabled = driver.findElement(By.xpath(Locators.valueOf(locator).getXpath())).isEnabled();
        assertTrue(displayed);
        assertTrue(enabled);
    }

    @Then("Verify new opened page")
    public void verifyNewOpenedPage() {
        String pageTitle = driver.getTitle();
        assertFalse(pageTitle.equals("Google"));
    }

    @Then("Check visibility apps and names")
    public void checkVisibilityAppsAndNames() {
        driver.switchTo().frame(driver.findElement(By.xpath("//iframe")));
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.LIST_APPS_GOOGLE1.getXpath())));
        logic.checkVisibilityAppsAndNameObjects(driver, "LIST_APPS_GOOGLE1");
        logic.checkVisibilityAppsAndNameObjects(driver, "LIST_APPS_GOOGLE2");
    }

    @Then("Verify new opened page under {string}")
    public void verifyNewOpenedPageUnderSETTINGS(String locator) {
        assertTrue(logic.clickEachOptionAndVerifyNewPage(driver, locator));
    }
}
