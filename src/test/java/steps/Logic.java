package steps;

import org.openqa.selenium.*;

import java.util.List;

import static org.testng.Assert.*;

public class Logic {

    public void checkPopUpCookie(WebDriver driver){
        try {
            boolean exitElement = driver.findElement(By.xpath(Locators.POPUP_ADVERTISEMENT_AGR.getXpath())).isDisplayed();
            if (exitElement) {
                driver.findElement(By.xpath(Locators.POPUP_BUTTON_ACCEPT.getXpath())).click();
            }
        }
        catch (NoSuchElementException e){}
    }

    public void findValueFromListSearchAndClick(WebDriver driver, String name){
        List<WebElement> elements = driver.findElements(By.xpath(Locators.LIST_BOX_OPTIONS.getXpath()));
        boolean result = false;
        int count = 1;
        for(WebElement e: elements){
            String value = e.getText();
            count++;
                if(name.equals(value)) {
                    result =  true;
                    break;
                }
        }
        assertTrue(result);
        driver.findElement(By.xpath("//ul[@role = 'listbox']//div[" + count + "]/span")).click();
    }

    public void checkVisibilityAppsAndNameObjects(WebDriver driver, String listLocator){
        List<WebElement> elements = driver.findElements(By.xpath(Locators.valueOf(listLocator).getXpath()));
        boolean disp;
        String appName;
        for (WebElement e : elements) {
            disp = e.isDisplayed();
            appName = e.getText();
            assertTrue(disp);
            assertNotEquals(appName, "");
        }
    }

    public boolean clickEachOptionAndVerifyNewPage(WebDriver driver, String listLocator){
        String xpath = Locators.valueOf(listLocator).getXpath() + "/../ul/li";
        List<WebElement> elements = driver.findElements(By.xpath(xpath));
        boolean result = false;
        for (WebElement e : elements) {
            if(!e.getText().isEmpty()){
                if(e.isDisplayed())
                    result = true;
            }
        }
        return result;
    }
}
